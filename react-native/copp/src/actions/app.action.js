//@flow
import { appService } from '../services/app.service';

export const appAction = {
    sendToDataBase,
}

function sendToDataBase(latitude: any, longitude: any) {
    appService.sendingGeolocation(latitude, longitude).then(result => {
        // here is where we dispatch on redux and do all the redirections and 
        // load infos
        console.log(result);
    });
}