//@flow

import * as types from "../constants/actionTypes";
import type { ToggleLoading } from "../reducers/loading.reducer";

export function showLoading(): ToggleLoading {
    return { type: types.LOADING, loading: true };
}

export function hideLoading(): ToggleLoading {
    return { type: types.LOADING, loading: false };
}