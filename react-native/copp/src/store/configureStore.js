//@flow
import {createStore, applyMiddleware} from 'redux';
import rootReducer from '../reducers/rootReducer';
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk';

let middleware = [thunk];

if (__DEV__) {
    const reduxImmutableStateInvariant = require('redux-immutable-state-invariant').default();

    const logger = createLogger({collapsed: true});
    middleware = [...middleware, reduxImmutableStateInvariant, logger];
} else {
    middleware = [...middleware];
}

export default function configureStore(initialState: any) {
    return createStore(
        rootReducer,
        initialState,
        applyMiddleware(...middleware)
    );
}