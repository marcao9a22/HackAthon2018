import { Navigation } from 'react-native-navigation';

import App from '../App';


export function registerScreens(store: any, Provider: any) {
    Navigation.registerComponent('pbh.App', () => App, store, Provider);
}