import { URL_DOMAIN } from '../constants/url.constants';
import axios from 'axios';

export const appService = {
    sendingGeolocation
}

function sendingGeolocation(latitude: any, longitude: any) {
    return axios({
        method: 'post',
        url: `${URL_DOMAIN}`,
        timeout: 5000,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(function (response) {
        console.log(response)
    }).catch(function (error) {
        console.log(error)
    });
    
}