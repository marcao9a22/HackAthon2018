//@flow

import React, { PureComponent } from 'react'

import { StyleSheet, View, Image, ImageBackground } from 'react-native';

import BackgroundImage from "../../img/Login_Background.png";

export default class Background extends PureComponent<{}> {
    render() {

        return (
            <ImageBackground
                source={BackgroundImage}
                style={styles.cover}>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    cover: {
        flex: 1,
        position: 'absolute',
        width: "100%",
        height: "100%",
    },
})