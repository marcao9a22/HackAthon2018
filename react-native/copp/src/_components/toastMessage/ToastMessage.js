//@flow
import React, { PureComponent } from 'react';
import { ToastAndroid } from 'react-native';

export const Toast = {
    showMessage,
};

function showMessage(message: string) {
    ToastAndroid.showWithGravityAndOffset(
        message,
        ToastAndroid.LONG,
        ToastAndroid.BOTTOM,
        1,
        50
    );
}