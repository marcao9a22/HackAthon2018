//@flow
import * as types from '../constants/actionTypes';

export type ToggleLoading = { type: types.LOADING, loading: boolean };

type Action = ToggleLoading

type State = {
    loading: boolean
}

export default function (state: State = {loading: false}, action: Action): State {
    switch (action.type) {

        case types.LOADING:
            return {
                ...state,
                loading: action.loading
            };
        default:
            return state;
    }
}