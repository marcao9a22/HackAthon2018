/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */


import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  Image,
  View
} from 'react-native';
import { Provider } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import BackgroundImage from './src/_components/Background.js/Background';

import configureStore from "./src/store/configureStore";
import { registerScreens } from "./src/screens";
import { appAction } from "./src/actions/app.action";
import { Toast } from './src/_components/toastMessage/ToastMessage';

const store = configureStore();

registerScreens(store, Provider);

type Props = {
  navigator: any
};
type State = {
  latitude: number,
  longitude: number,
};
export default class App extends Component<Props, State> {
    constructor(props: Props) {
        super(props);

        this.State = {
          latitude: 0,
          longitude: 0,
      };

    }
    componentDidMount() {
      this.calculateGeolocation();
    }    
  
  calculateGeolocation = () => {
      setTimeout(function(){
          navigator.geolocation.getCurrentPosition(
              (position) => {
                  // here is where we send coords to action
                  appAction.sendToDataBase(position.coords.latitude, position.coords.longitude);
              },
              (error) => Toast.showMessage(`Não foi possível recuperar a posição do GPS [${error.message}]. Estamos fazendo o possivel para conseguir!`),
              {enableHighAccuracy: false, timeout: 20000, maximumAge: 1000},
          );
      }, 3000);
  }

  render() {
    
    return (
      <View style={styles.container}>
      <BackgroundImage/>
      <Image source={require('./src/img/logo.png')} />
        <Text style={styles.welcome}>
          Seja bem vindo ao COPP localiza.
        </Text>
        <Text style={styles.instructions}>
          Espere só um pouquinho, estamos sincronizando as coisas. :)
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: '#fff'
  },
  instructions: {
    textAlign: 'center',
    color: '#fff',
    marginBottom: 5,
    
  },
});
