<!DOCTYPE html>
<html>
  <head>
  	<?php header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, PUT');?>
    <meta charset="utf-8">
    <title>AteCubanos<code>setTimeout()</code></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
      #map {
        height: 100%;
        overflow: hidden !important;
      }

      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #floating-panel {
        position: absolute;
        top: 10px;
        left: 25%;
        background-color: #fff;
        padding: 5px;
        border: 1px solid #999;
        text-align: center;
        font-family: 'Roboto','sans-serif';
        line-height: 30px;
        padding-left: 10px;
      }
      #floating-panel {
        margin-left: -52px;
      }
      .modalc{
        padding: 10px;
        padding-top: 30px;
      }
      input{
        padding: 10px;
        width: 100%;
        margin-bottom: 10px;
      }

      .pronto{
        text-align: center;
      }

      select{
      	padding-top: 10px;
      	margin-bottom: 3px;
      }

      #userim{
      	margin-left: 45px;
      	border-radius: 50%;
      }



    </style>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </head>
  <body>
    <div id="map">
    </div>

  
  
    

    <script>

    var neighborhoods;
    var markers = [];
    var map;
    var escolhido = 9999999;

    $(document).ready(function(){

        $.ajax({
            url: "php/savelatlon.php",
            method:"GET",
            contentType:false,
            cache:false,
            processData:false,
            success:function(data){
                neighborhoods = JSON.parse(data);
                marcar(neighborhoods);
            },
            error: function(e){
              alert("problemas no servidor");    
            }
        });
    });


    function marcar(data){
        console.log(data);
         clearMarkers();    
        for (var i = 0; i < data.length; i++) {
          addMarkerWithTimeout(data[i], i * 200);
        }
    }


    function CenterControlModal(controlDiv, map){


    var modal = document.createElement('div');
  modal.style.background = 'rgb(171, 172, 173, 0.5)';
  modal.style.border = '2px solid rgb(171, 172, 173, 0.5)';
  modal.style.borderBottom = 'none';
  modal.style.borderRadius = '5px';
  modal.style.height = '350px';
  modal.style.width = '300px';
  modal.innerHTML = '<div class="modalc" id="modalcc"><label for="usr">Bairro:</label><input value="Alto dos Pinheiros" type="text" class="form-control" id="bairro"><label for="usr">Rua:</label><input value="Vesta" type="text" class="form-control" id="rua"><label for="usr">Numero:</label><input type="text" class="form-control" id="numero" value="117"><label for="usr">Tipo:</label><select class="form-control" id="slctipo"><option value="policia">flanelinha</option><option value="bombeiro">queda de arvore</option><option value="policia">furto de cabos</option><option value="bombeiro">enchente</option><option value="lixeiro">lixo na rua</option><option value="samu">acidente</option></select><button class="btn btn-success pronto" onclick="pronto();">Pronto!</button></div>';
  controlDiv.appendChild(modal);

    }


    function CenterControl(controlDiv, map) {

  // Set CSS for the control border.
  var controlUI = document.createElement('div');
  controlUI.style.backgroundColor = '#f44242';
  controlUI.style.border = '2px solid #f44242';
  controlUI.style.borderRadius = '3px';
  controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
  controlUI.style.cursor = 'pointer';
  controlUI.style.marginBottom = '22px';
  controlUI.style.textAlign = 'center';
  controlUI.title = 'Click to recenter the map';
  controlDiv.appendChild(controlUI);

  // Set CSS for the control interior.
  var controlText = document.createElement('div');
  controlText.style.color = 'rgb(255, 255, 255)';
  controlText.style.background = 'rgb(244, 66, 66)';
  controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
  controlText.style.fontSize = '16px';
  controlText.style.lineHeight = '38px';
  controlText.style.paddingLeft = '5px';
  controlText.style.paddingRight = '5px';
  controlText.innerHTML = 'Realizar solicitação';
  controlUI.appendChild(controlText);

  // Setup the click event listeners: simply set the map to Chicago.
  controlUI.addEventListener('click', function() {
      var centerControlDivmodal = document.createElement('div');
          var centerControl = new CenterControlModal(centerControlDivmodal, map);

          centerControlDivmodal.index = 1;
          map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDivmodal);
  });

}

      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: {lat: -19.9191248, lng: -43.9408178}
        });

        var centerControlDiv = document.createElement('div');
          var centerControl = new CenterControl(centerControlDiv, map);

          centerControlDiv.index = 1;
          map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);
      }


      function addMarkerWithTimeout(position, timeout) {
        window.setTimeout(function() {
          markers.push(new google.maps.Marker({
            position: {lat: parseFloat(position['lat']), lng: parseFloat(position['lng'])},
            map: map,
            icon: position['img'],
            title: position['nome'],
            animation: google.maps.Animation.DROP
          }));
        }, timeout);
      }


      function clearMarkers() {
        for (var i = 0; i < markers.length; i++) {
          markers[i].setMap(null);
        }
        markers = [];
      }



      function pronto(){
        address = "Rua "+$("#rua").val()+", "+$("#numero").val()+", "+$("#bairro").val()+", Belo Horizonte - MG";
      
      $.ajax({
        url : 'http://maps.googleapis.com/maps/api/geocode/json',
        dataType : 'json',  
        type : 'GET',
        data : {
          address : address,
          sensor : false
        },
        success : function( data ) {
        	calcula(data.results[0].geometry.location.lat, data.results[0].geometry.location.lng, $('#slctipo option:selected').val());
        }
      });
      }


      function calcula(lat, lng, tipo){
      	console.log(lat);
      	console.log(lng);
      	console.log(tipo);

      	$.ajax({
        url : "http://localhost/hackathon/php/conta.php?",
        dataType : 'json',  
        type : 'GET',
        data : {
          lat1 : lat,
          long1 : lng,
          tipo : tipo
        },
        success : function( data ) {
        	$("#modalcc").html("");	
        	$("#modalcc").append("<img id='load' src='https://cdn-images-1.medium.com/max/1600/1*W8cj-FRc58UozzcMWqVPZw.gif' width='275px'/>");
        	setTimeout(function(){ $("#modalcc").html("");
        		$("#modalcc").append("<img id='userim' src='https://www.w3schools.com/howto/img_avatar.png' alt='Avatar' style='width:150px'><br>");
        		$("#modalcc").append("<h6>Nome: "+data.nome+"</h6>");
        		$("#modalcc").append("<h6>Cod: 19482934</h6>");
        		$("#modalcc").append("<h6>Celular: (31)99463657</h6>");
        		$("#modalcc").append("<h6>O agente foi alertado! E já está se dirigindo ao local.</h6>");
        		
        	}, 2000);

        }
      });
      }

     
      



    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxOYXjkc5I-30Hr5Ml0DdYkXywel_Ym9M&callback=initMap">
    </script>
  </body>
</html>