 <?php
header('Access-Control-Allow-Origin: *');
 require_once '../class/class.Conn.php';
 $PDO = Database::conexao();

 $lat1 = $_GET['lat1'];
 $long1 = $_GET['long1'];

 $tipo = $_GET['tipo'];

 $arrayusuario;

 $escolhido = 999999;



$sql = "SELECT * FROM `usuario` WHERE `tipo` = :tipo";

$stmt = $PDO->prepare($sql);
$stmt->bindParam(':tipo', $tipo);
$stmt->execute();
$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
if(!$result) {
    echo false;
}else{
    $arrayusuario = $result;
    //echo json_encode($result);
}

 for ($i=0; $i < count($arrayusuario); $i++) { 

    $url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins=".$lat1.",%20".$long1."&destinations=".$arrayusuario[$i]['lat'].",%20".$arrayusuario[$i]['lng']."&mode=driving&language=pl-PL";


   //  Initiate curl
$ch = curl_init();
// Disable SSL verification
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// Will return the response, if false it print the response
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Set the url
curl_setopt($ch, CURLOPT_URL,$url);
// Execute
$result=curl_exec($ch);
// Closing
curl_close($ch);
$response_a = json_decode($result, true);
$dist = $response_a['rows'][0]['elements'][0]['distance']['value'];


if ($dist < $escolhido) {
    $escolhido = $dist;
    $retorno = json_encode($arrayusuario[$i]);
}

}

echo $retorno;

?>