<?php
/**
 * require_once './conn.class.php';
 * $db = Database::conexao();
 */
class Database
{
    protected static $PDO;
    private function __construct()
    {
        $db_host = "localhost";
        $db_nome = "hackathon";
        $db_usuario = "root";
        $db_senha = "";
        $db_driver = "mysql";
        try
        {
            # Atribui o objeto PDO à variável $PDO.
            self::$PDO = new PDO("$db_driver:host=$db_host; dbname=$db_nome", $db_usuario, $db_senha);
            # Garante que o PDO lance exceções durante erros.
            self::$PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            # Garante que os dados sejam armazenados com codificação UFT-8.
            self::$PDO->exec('SET NAMES utf8');
        }
        catch (PDOException $e)
        {
            die("Connection Error: " . $e->getMessage());
        }
    }
    public static function conexao()
    {
        if (!self::$PDO)
        {
            new Database();
        }
        return self::$PDO;
    }
}